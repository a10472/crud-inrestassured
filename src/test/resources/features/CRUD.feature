Feature: CRUD Issues

  Scenario: CRUD Operations for an Issue
    Given I create an Issue
    Then Issue is created
    When I retrieve the Issue
    Then Issue is retrieved
    When I update the Issue
    Then Issue is updated
    When I delete the Issue
    Then Issue is deleted

  Scenario: Edge Case - When User is Not Authorized
    Given User is UnAuthorized
    Then Issue is Not created

  Scenario: Edge Case - Pass Incorrect Values in Request Parameter
     Given I create an Issue with Incorrect values
    Then Issue is Not created due to validations

  Scenario: Edge Case - Try to Create Duplicate Issue
    Given I create an Issue
    Then Issue is created
    When I try to Create an Issue with Same Issue_IID
    Then I get Duplicate Issue Validation

  Scenario: Edge Case - Add a ToDo to an Issue
    Given I create an Issue
    Then Issue is created
    When I try to Add a ToDo to an Issue
    Then a ToDo is Added
